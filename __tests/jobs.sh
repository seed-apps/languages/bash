#
# programme simple
# =================================
#
# executer la fonction test1
# * avec differents arguments en parallèle
# * dans différents scenarios
#

#debut
#----------------------------------
DEBUG true
START

function test1() {
    sleep $1
    echo "$2"
}


TITLE "stop when tsks finished"
#----------------------------------

JOB test1 1.0 "a"
JOB test1 1.5 "b"

JOBS_LS
JOBS_WAIT
JOBS_LS

TITLE "stop and kill"
#----------------------------------

JOB test1 100.0 "a"
JOB test1 100.0 "b"

sleep 3.0
JOBS_LS
JOBS_KILL
JOBS_LS

TITLE "stop when key pushed"
#----------------------------------
JOB test1 1.0 "a"
JOB test1 1.5 "b"
JOB test1 10.5 "c"

ENTER_TO_CONTINUE
JOBS_KILL
ERROR 2 "agrrr"

#fin
#----------------------------------
EXIT
