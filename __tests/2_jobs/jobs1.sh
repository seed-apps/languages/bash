
function test() {

    sleep $1
    echo "sleeping $1 s"
}

START
JOBS_LS
JOB "test 2.0"
JOB "test 5.0"
JOBS_LS
sleep 1.0
JOBS_KILL
JOBS_LS
JOBS_WAIT
EXIT
