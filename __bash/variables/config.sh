#==============================================================   

function GLOBAL_FILE () {

#==============================================================   
#
# load a file with variables and add a prefix
#
#==============================================================   


    local filename="$1"
    local prefix="$2"


    if [ -f "$filename" ]
    then
        while IFS="=" read k v
        do
            if [ ! -z "$k" ]
            then
                GLOBAL $prefix"_$k" "$v"
                
            fi

        done < "$filename"
    else
        MESSAGE "no conf $@"
    fi

} 
#==============================================================   
