

#==============================================================

function PYTHONPATH() {

#==============================================================

    local dirpath="$1"
    if [ -d "$dirpath" ]
    then
        for elt in $(find $dirpath -type d -name "__python3")
        do
            #echo "# PYTHONPATH $elt"
            export PYTHONPATH="$elt:$PYTHONPATH"
        done
    fi
}
#==============================================================

function PATH() {

#==============================================================

    local dirpath="$1"
    if [ -d "$dirpath" ]
    then
        #echo "# PATH $dirpath"
        for binpath in $(find $dirpath -type d -name "__bin")
        do
            #echo "# PATH $binpath"
            for elt in $(find $binpath -type f)
            do
                chmod +x $elt
            done
            export PATH="$binpath:$PATH"
        done

    fi
}
#==============================================================

