######################################################

function CP() {

######################################################

    SUBLIST "CP"
    SUBSUBLIST "source : $1"
    SUBSUBLIST "dest   : $2"

    if [ -d "$1" ]
    then
        DIR "$2"
        rsync -az --delete-after $1/ $2

    elif [ -f "$1" ]
    then
        cp "$1" "$2" 
    fi

}


######################################################

function RSYNC_LOCAL_ADD() {

######################################################


    SUBLIST "RSYNC_LOCAL_ADD"
    SUBSUBLIST "source : $1"
    SUBSUBLIST "dest   : $2"


    if [ -z "$2" ]
    then
        ERROR "no output directory"


    elif [ -z "$1" ]
    then
        ERROR "no input directory $1"
    else

        COMMAND rsync -a --ignore-existing $1/* $2
    fi

}

######################################################

function RSYNC_LOCAL_MIRROR() {

######################################################
    
    if [ -z "$2" ]
    then
        ERROR "no output directory $@"
    elif [ ! -d "$1" ]
    then
        ERROR "no input directory $@"
    else


        #SUBLIST "RSYNC_LOCAL_MIRROR"
        #SUBSUBLIST "source : $1"
        #SUBSUBLIST "dest   : $2"

        rsync -az --exclude '__pycache__' --exclude '*.pyc' --delete-after $1/ $2

    fi
}
######################################################

function RSYNC_NT_MIRROR() {

######################################################
    
    if [ -z "$2" ]
    then
        ERROR "no output $2"
    elif [ -z "$1" ]
    then
        ERROR "no input $1"
    else

        MESSAGE "RSYNC_NT_MIRROR"
        MESSAGE "source : $1"
        MESSAGE "dest   : $2"

        COMMAND rsync -e ssh -az  --exclude '__pycache__' --exclude '*.pyc' --delete-after $1/ $2

    fi
}
######################################################
