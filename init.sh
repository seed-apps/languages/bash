#!/bin/bash
#
# installation de seed
#
#================================================
#       fonctions
#================================================
#
#------------------------------------------------
function CONFIG(){
#------------------------------------------------
# créer le fichier de configuration
#
#------------------------------------------------

    if [ -z "$SEED_DIR" ]
    then
        export SEED_DIR="$(realpath ./..)"
    fi
    export SEED_CONF="$SEED_DIR/env/config"


    if [ ! -d "$(dirname $SEED_CONF)" ]
    then
        local path=$(dirname $SEED_CONF)
        mkdir -p $path
        #chown $USER:$USER $path
    fi

    if [ ! -f "$SEED_CONF" ]
    then
        touch $SEED_CONF
    fi
    echo "#!/bin/bash" > $SEED_CONF
    echo "#création automatique" >> $SEED_CONF
    echo "#voir main/install.sh" >> $SEED_CONF
}
#------------------------------------------------
function CONFIG_ADD(){
#------------------------------------------------

   cat $1 >> $SEED_CONF
}
#------------------------------------------------
function CONFIG_VAR(){
#------------------------------------------------
# ajouter une variable
#
#------------------------------------------------

    local var="$1"
    shift 1
    local data="$@"
    string="export $var=\"$data\""
    eval "$string"
    echo "$string" >> $SEED_CONF
}
#------------------------------------------------
function CONFIG_PATH(){
#------------------------------------------------
# ajouter une variable
#
#------------------------------------------------

    local data="$1"

    echo "export PATH=$data:\$PATH" >> $SEED_CONF
    
    for script in $(find $data -mindepth 1 -type f)
    do
        chmod +x $script
    done

}
#------------------------------------------------
function CONFIG_PYTHONPATH(){
#------------------------------------------------
# ajouter une variable
#
#------------------------------------------------

    local data="$1"

    echo "export PYTHONPATH=$data:\$PYTHONPATH" >> $SEED_CONF
    

}
#------------------------------------------------
function CONFIG_DIR(){
#------------------------------------------------
# ajouter un répertoire
#
#------------------------------------------------

    local var="$1"
    local path="$2"

    if [ ! -d "$path" ]
    then
        mkdir -p $path
    fi
    CONFIG_VAR "$var" "$path"
}
#------------------------------------------------
function CONFIG_STORE(){
#------------------------------------------------
# ajouter un répertoire
#
#------------------------------------------------

    local var="$1"
    local path="$2"

    mkdir -p $path
    #chown $USER:$USER $path
    CONFIG_VAR "$var" "$path"
}
#------------------------------------------------
function BASHRC_INIT(){
#------------------------------------------------
# ajouter le fichier de conf au bashrc 
# pour l'utilisateur
# relancer le terminal pour importer l'environnement
#------------------------------------------------

    local bashrc_user="$HOME/.bashrc"
    local bashrc_seed="$SEED_ENV/etc/bashrc-backup"

    if [ ! -f "$bashrc_seed" ]
    then
        cp $bashrc_user $bashrc_seed
    else
        cp $bashrc_seed $bashrc_user
    fi

    echo ". $SEED_CONF" >> $bashrc_user
}
#------------------------------------------------

#================================================
#création du fichier de configuration
#================================================

CONFIG


CONFIG_VAR "SEED_GIT" "git@framagit.org:seed"
CONFIG_VAR "SEED_DIR" "$SEED_DIR"
CONFIG_VAR "SEED_MAIN" "$SEED_DIR/seed-bash"
CONFIG_VAR "SEED_IMPORT" "$SEED_DIR/main/import.sh"
CONFIG_VAR "SEED_CONF" "$SEED_CONF"

CONFIG_STORE "SEED_ENV" "$SEED_DIR/env"
CONFIG_STORE "SEED_STORE" "$SEED_DIR-projects"

#script="$SEED_ENV/root/env.sh"
#echo "$script"
#cat "$script"
#source "$script"



    for lib in $(find "$SEED_DIR" -mindepth 1 -type d -name "__python3" | sort -V)
    do

        CONFIG_PYTHONPATH $lib
    done
    


for directory in $(find $SEED_DIR -mindepth 1 -type d -name "__bin" | sort -V)
do
    CONFIG_PATH "$directory"
done



CONFIG_ADD "$SEED_MAIN/functions.sh"



#BASHRC_INIT

cat "$SEED_ENV/config"
#================================================

