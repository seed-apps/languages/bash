
file pathes : manipulation des chemins
====================================================


chemin relatif ::

   path_relative [path_to_file] [root_path]
   
chemin absolu ::

   path_real [path_to_file]
   
nom ::

   path_name [path_to_file]
   
parent ::

   path_parent [path_to_file]
   
extention ::

   path_extention [path_to_file]
   