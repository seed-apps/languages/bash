
file listing : repertoires et fichiers
====================================================

lister les répertoires ::

   #lister les sous-répertoires
   ls_dir [dir_path]
   
   #récursif
   ls_dir [dir_path] -r

   
lister les fichiers ::

   #lister les fichiers
   ls_file [dir_path]
   
   #récursif
   ls_file [dir_path] -r
   
   
lister les fichiers avec une extention donnée ::

   #lister les fichiers
   filter_file [dir_path]
   
   #récursif
   filter_file [dir_path] -r
   
lister les fichiers et répertoires avec dont le nom contient la chaine de caratère donnée ::

   #lister les fichiers
   ls_name [dir_path] [string]
   
   #récursif
   ls_name [dir_path] [string] -r
   
liste les packages. par exemple, les packages python contiennent le fichier __init__.py ::

    #sous répertoires
    ls_package [dir_path] [file_name]
    
    #récursif
    ls_package [dir_path] [file_name] -r
    

