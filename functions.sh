alias @cd='cd $SEED_DIR'
alias @p='cd $SEED_STORE'
alias @sys='cd $SEED_ENV'
alias @ls='tree -L 2 -d'
alias @init=' @env init'
alias @conf=' @env config'
alias @pull=' @env pull'
alias @push=' @env push'

for directory in $(find $SEED_DIR -mindepth 1 -type d -name "__bash")
do
    for script in $(find $directory -mindepth 1 -type f)
    do
        source "$script"
    done
done
#DEBUG true
for script in $(find $SEED_DIR -mindepth 1 -type f -name "__globals__.sh" | sort -V)
do
    export APP_PATH="$(dirname $script)"
    MESSAGE "SCRIPT $script"
    #cat "$script"
    source "$script"
done
